/* global google */

var lbgs_autocomplete, lbgs_autocomplete_map, lbgs_autocomplete_marker;

function lbgs_init() {

   lbgs_autocomplete = new google.maps.places.Autocomplete((document.getElementById('lbgs_post_address')), {types: ['geocode']});
   lbgs_autocomplete.addListener('place_changed', lbgs_fillInAddress);

   lbgs_initMap();
}

function lbgs_fillInAddress() {

   var place = lbgs_autocomplete.getPlace();

   var lat = place.geometry.location.lat(),
           lng = place.geometry.location.lng();

   console.log("lbgs_fillInAddress lat=" + lat + ", lng=" + lng);

   document.getElementById("lbgs_post_address_lat").value = lat;
   document.getElementById("lbgs_post_address_lng").value = lng;

   lbgs_centerMap(lat, lng);
}

function lbgs_initMap() {

   var lat = parseFloat(document.getElementById("lbgs_post_address_lat").value),
           lng = parseFloat(document.getElementById("lbgs_post_address_lng").value);

   console.log("lbgs_initMap lat=" + lat + ", lng=" + lng);

   var markerFlag = !lat || !lat;

   if (!lat)
      lat = 41.909986;

   if (!lng)
      lng = 12.3959136;

   lbgs_autocomplete_map = new google.maps.Map(document.getElementById('lbgs_post_address_map'), {
      zoom: 3
   });

   lbgs_centerMap(lat, lng);

   if (markerFlag)
      lbgs_autocomplete_marker.setMap(null);
}

function lbgs_centerMap(lat, lng) {

   var pos = {lat: lat, lng: lng};

   if (lbgs_autocomplete_marker) {
      lbgs_autocomplete_marker.setMap(null);
   }

   lbgs_autocomplete_marker = new google.maps.Marker({
      position: pos,
      clickable: false,
      map: lbgs_autocomplete_map,
      animation: google.maps.Animation.DROP
   });

   if (lbgs_autocomplete_map) {
      lbgs_autocomplete_map.setCenter(pos);
   }
}


