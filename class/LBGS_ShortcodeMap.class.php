<?php

/**
 * @author Luca Bonaldo <luca.bonaldo.lb@gmail.com>
 */
class LBGS_ShortcodeMap {

   private $gmap_key;

   public function __construct() {
      add_shortcode('geo-shop-map', array(&$this, 'get_shortcode'));
      $this->gmap_key = get_option(OPT_GMAPS_KEY, FALSE);

      if ($this->gmap_key) {
         add_action("wp_footer", array($this, "lbgs_print_gmaps_script"));
      }
   }

   public function lbgs_print_gmaps_script() {
      ?> 
      <!-- GMAP -->
      <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->gmap_key; ?>&callback=lbgs_init" async defer></script>
      <?php
   }

   public function get_shortcode($atts) {

      extract(shortcode_atts(array(
          'height' => '500px',
          'width' => '700px',
          'geoloc' => 'Y',
          'icon' => ''
                      ), $atts));

      echo "<!-- LBGS -->";
      echo "<!-- height=$height | width=$width | geoloc=$geoloc | icon=$icon  -->";
      ?>
      <script type="text/javascript">

         var lbgs_map, lbgs_markers = [], lbgs_windows = [], lbgs_icon = false;

      <?php if ($icon) { ?>
            lbgs_icon = {icon: "<?php echo $icon; ?>"};
      <?php } ?>

         function lbgs_init() {

            lbgs_map = new google.maps.Map(document.getElementById('lbgs_map'), {zoom: 3});
            lbgs_map.setCenter({lat: 41.909986, lng: 12.3959136});

      <?php if (strtoupper($geoloc) == 'Y') { ?>
               if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function (position) {
                     lbgs_map.setCenter({
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                     });
                  });
               }
      <?php } ?>


      <?php
      global $wpdb;

      $results = $wpdb->get_results("SELECT ID, post_title, post_content FROM " . $wpdb->posts . " WHERE post_type = '" . SHOP_POST_TYPE . "' AND post_status = 'publish';");

      foreach ($results as $post) :

         $lat = get_post_meta($post->ID, '_lbgs_post_address_lat', true);
         $lng = get_post_meta($post->ID, '_lbgs_post_address_lng', true);

         if ($lat && $lng) {
            ?>
                  lbgs_windows["<?php echo $post->ID; ?>"] = new google.maps.InfoWindow({
                     content: "<?php echo $this->prepare_windows_content($post); ?>"
                  });

                  lbgs_markers["<?php echo $post->ID; ?>"] = new google.maps.Marker({
                     position: {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>},
                     map: lbgs_map,
                     animation: google.maps.Animation.DROP
                  });

                  lbgs_markers["<?php echo $post->ID; ?>"].addListener("click", function () {
                     lbgs_windows["<?php echo $post->ID; ?>"].open(lbgs_map, lbgs_markers["<?php echo $post->ID; ?>"]);
                  });
            <?php
         }

      endforeach;

      wp_reset_postdata();
      ?>
            if (lbgs_icon) {
               for (var i in lbgs_markers) {
                  lbgs_markers[i].setOptions(lbgs_icon);
               }
            }
         }
      </script>
      <?php
      echo "<!-- ./LBGS -->";
      return sprintf("<div id='lbgs_map' style='width: %s; height: %s;'></div>", $width, $height);
   }

   private function prepare_windows_content($post) {

      $windowsContentTPL = '<div class=\"lbgs_windows\">'
              . '      <h3 class=\"lbgs_windows_title\">%s</h3>'
              . '      <div class=\"lbgs_windows_body\">'
              // . '        <p>%s</p>'
              . '        <p>'
              . '          <ul style=\"list-style:none;padding:0;margin:0;\">%s</ul>'
              . '        </p>'
              . '      </div>'
              . '    </div>';

      $li = "";

      $phone = get_post_meta($post->ID, '_lbgs_post_phone', true);
      if ($phone) {
         $li .= "<li><b>" . __("Telefono", TEXT_DOMAIN) . "</b>: " . esc_html($phone) . "</li>";
      }

      $mail = get_post_meta($post->ID, '_lbgs_post_mail', true);
      if ($mail) {
         $li .= "<li><b>Mail</b>: <a href=mailto:" . esc_attr($mail) . ">" . esc_html($mail) . "</a></li>";
      }

      $address = get_post_meta($post->ID, '_lbgs_post_address', true);
      if ($address) {
         $li .= "<li><b>" . __("Indirzzo", TEXT_DOMAIN) . "</b>: " . esc_html($address) . "</li>";
      }

      return sprintf($windowsContentTPL, esc_html($post->post_title), $li);
   }

}

$LBGS_ShortcodeMap = new LBGS_ShortcodeMap();
