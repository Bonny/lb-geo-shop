<?php

/**
 * @author Luca Bonaldo <luca.bonaldo.lb@gmail.com>
 */
class LBGS_PostType {

   private $gmap_key;

   public function __construct() {

      add_action('init', array($this, 'lbgs_create_post_type'));
      add_filter('manage_' . SHOP_POST_TYPE . '_posts_columns', array($this, 'lbgs_post_columns_head'));
      add_action('manage_' . SHOP_POST_TYPE . '_posts_custom_column', array($this, 'lbgs_post_columns_content'), 10, 2);
      add_filter('enter_title_here', array($this, 'lbgs_change_default_title'));
      add_action('save_post', array($this, 'lbgs_post_save_meta'), 1, 2);
      add_action('admin_enqueue_scripts', array($this, 'lbgs_admin_enqueue'));

      $this->gmap_key = get_option(OPT_GMAPS_KEY, FALSE);

      if ($this->gmap_key) {
         add_action("admin_footer", array($this, "lbgs_print_admin_gmaps_script"));
      } else {
         add_action('admin_notices', array($this, 'lbgs_admin_notice_error'));
      }
   }

   public function lbgs_admin_enqueue($hook) {
      if ($hook == 'post.php' || $hook == 'post-new.php') {
         wp_enqueue_script('lbgs_post_type', PLUGIN_DIR . 'js/post_type.js');
      }
   }

   public function lbgs_create_post_type() {

      $labels = array(
          'name' => __('Punti vendita', TEXT_DOMAIN),
          'singular_name' => __('Punto vendita', TEXT_DOMAIN),
          'add_new' => __('Aggiungi punto vendita', TEXT_DOMAIN),
          'all_items' => __('Tutti i punti vendita', TEXT_DOMAIN),
          'add_new_item' => __('Aggiungi nuovo punto vendita', TEXT_DOMAIN),
          'edit_item' => __('Modifica', TEXT_DOMAIN),
          'new_item' => __('Nuovo punto vendita', TEXT_DOMAIN),
          'view_item' => __('Visualizza punto vendita', TEXT_DOMAIN),
          'search_items' => __('Cerca punto vendita', TEXT_DOMAIN),
          'not_found' => __('Nessun punto vendita trovato', TEXT_DOMAIN),
          'not_found_in_trash' => __('Nessun punto vendita nel cestino', TEXT_DOMAIN),
          'parent_item_colon' => __('Parent punto vendita', TEXT_DOMAIN)
              //'menu_name' => default to 'name'
      );

      $args = array(
          'labels' => $labels,
          'public' => true,
          'has_archive' => false,
          'publicly_queryable' => true,
          'query_var' => true,
          'rewrite' => true,
          'capability_type' => 'post',
          'hierarchical' => false,
          'supports' => array('title'/*, 'editor' , 'thumbnail' */),
          'taxonomies' => array(), // add default post categories and tags
          'menu_position' => 5,
          'menu_icon' => 'dashicons-location-alt',
          'exclude_from_search' => false,
          'register_meta_box_cb' => array($this, 'lbgs_add_post_type_metabox')
      );

      register_post_type(SHOP_POST_TYPE, $args);
      //flush_rewrite_rules();
   }

   public function lbgs_add_post_type_metabox() {
      add_meta_box('geo-shop-metabox', 'Dettaglio', array($this, 'lbgs_post_type_metabox'), SHOP_POST_TYPE, 'normal');
   }

   public function lbgs_post_type_metabox() {

      global $post;

      echo '<input type="hidden" name="lbgs_post_noncename" value="' . wp_create_nonce(plugin_basename(__FILE__)) . '" />';
      ?>

      <table class="form-table" style="width: 50%;float: left;">
         <tr>
            <th>
               <label><?php _e("Telefono", TEXT_DOMAIN); ?></label>
            </th>
            <td>
               <input type="text" name="lbgs_post_phone" class="regular-text" value="<?php echo get_post_meta($post->ID, '_lbgs_post_phone', true); ?>"> 
            </td>
         </tr>
         <tr>
            <th>
               <label>Email</label>
            </th>
            <td>
               <input type="text" name="lbgs_post_mail" class="regular-text" value="<?php echo get_post_meta($post->ID, '_lbgs_post_mail', true); ?>"> 
            </td>
         </tr>
         <tr>
            <th>
               <label><?php _e("Indirizzo", TEXT_DOMAIN); ?></label>
            </th>
            <td>
               <input type="text" name="lbgs_post_address" id="lbgs_post_address" class="regular-text" value="<?php echo get_post_meta($post->ID, '_lbgs_post_address', true); ?>"> 
            </td>
         </tr>
      </table>

      <div id="lbgs_post_address_map" style="height: 300px;width: 50%; margin-top: 2em;"></div>

      <input type="hidden" name="lbgs_post_address_lat" id="lbgs_post_address_lat" value="<?php echo get_post_meta($post->ID, '_lbgs_post_address_lat', true); ?>">
      <input type="hidden" name="lbgs_post_address_lng" id="lbgs_post_address_lng" value="<?php echo get_post_meta($post->ID, '_lbgs_post_address_lng', true); ?>">

      <?php
   }

   public function lbgs_post_columns_head($defaults) {
      $defaults['lbgs_post_address'] = __("Indirizzo", TEXT_DOMAIN);
      $defaults['lbgs_post_phone'] = __("Telefono", TEXT_DOMAIN);
      $defaults['lbgs_post_mail'] = 'Email';
      return $defaults;
   }

   public function lbgs_post_columns_content($column_name, $post_ID) {

      if ($column_name == 'lbgs_post_address' || $column_name == 'lbgs_post_phone') {
         echo get_post_meta($post_ID, '_' . $column_name, true);
      } else if ($column_name == 'lbgs_post_mail') {
         $mail = get_post_meta($post_ID, '_' . $column_name, true);
         if ($mail) {
            echo sprintf("<a href='mailto:%s'>%s</a>", $mail, $mail);
         }
      }
   }

   public function lbgs_change_default_title($title) {
      $screen = get_current_screen();
      if (SHOP_POST_TYPE == $screen->post_type) {
         $title = 'Inserisci qui il nome del negozio';
      }
      return $title;
   }

   public function lbgs_post_save_meta($post_id, $post) {
      if (!isset($_POST['lbgs_post_noncename'])) { // Check if our nonce is set.
         return;
      }

      if (!wp_verify_nonce($_POST['lbgs_post_noncename'], plugin_basename(__FILE__))) {
         return $post->ID;
      }

      if (!current_user_can('edit_post', $post->ID)) {
         return $post->ID;
      }

      $lbgs_post_meta['_lbgs_post_phone'] = $_POST['lbgs_post_phone'];
      $lbgs_post_meta['_lbgs_post_mail'] = $_POST['lbgs_post_mail'];
      $lbgs_post_meta['_lbgs_post_address'] = $_POST['lbgs_post_address'];
      $lbgs_post_meta['_lbgs_post_address_lat'] = $_POST['lbgs_post_address_lat'];
      $lbgs_post_meta['_lbgs_post_address_lng'] = $_POST['lbgs_post_address_lng'];

      // add values as custom fields
      foreach ($lbgs_post_meta as $key => $value) { // cycle through the $lbgs_post_meta array
         // if( $post->post_type == 'revision' ) return; // don't store custom data twice
         $value = implode(',', (array) $value); // if $value is an array, make it a CSV (unlikely)
         if (get_post_meta($post->ID, $key, FALSE)) { // if the custom field already has a value
            update_post_meta($post->ID, $key, $value);
         } else { // if the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
         }
         if (!$value) { // delete if blank
            delete_post_meta($post->ID, $key);
         }
      }

//      if (!empty($lbgs_post_meta['_lbgs_post_address_lat']) && !empty($lbgs_post_meta['_lbgs_post_address_lng'])) {
//
//         global $wpdb;
//         $table = $wpdb->prefix . ADDRESS_TABLE;
//
//         if ($wpdb->get_row("SELECT * FROM $table WHERE post_id = $post->ID") == NULL) {
//
//            $wpdb->insert($table, array(
//                'post_id' => $post->ID,
//                'lat' => $lbgs_post_meta['_lbgs_post_address_lat'],
//                'lng' => $lbgs_post_meta['_lbgs_post_address_lng']
//                    ), array('%d', '%f', '%f')
//            );
//         } else {
//
//            $wpdb->update($table, array(
//                'lat' => $lbgs_post_meta['_lbgs_post_address_lat'],
//                'lng' => $lbgs_post_meta['_lbgs_post_address_lng']
//                    ), array('post_id' => $post->ID), array('%f', '%f'), array('%d')
//            );
//         }
//      }
   }

   public function lbgs_print_admin_gmaps_script() {
      if ($this->canAddScript()) {
         ?> 
         <!-- GMAP -->
         <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->gmap_key; ?>&libraries=places&callback=lbgs_init" async defer></script>
         <?php
      }
   }

   public function lbgs_admin_notice_error() {
      printf('<div class="notice notice-warning is-dismissible"><p>%s</p></div>', __('Irks! An error has occurred.', TEXT_DOMAIN));
   }

   private function canAddScript() {
      $screen = get_current_screen();
      return $screen->post_type == SHOP_POST_TYPE && $screen->base == 'post' && $screen->parent_base == 'edit';
   }

}

$LBGS_PostType = new LBGS_PostType();
