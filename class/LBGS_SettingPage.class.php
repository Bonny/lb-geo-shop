<?php

/**
 * @author Luca Bonaldo <luca.bonaldo.lb@gmail.com>
 */
class LBGS_SettingPage {

   public function __construct() {
      add_action('admin_menu', array(&$this, 'lbgs_plugin_create_menu'));
   }

   public function lbgs_plugin_create_menu() {
      add_options_page('Settings Admin', 'LB Geo Shop', 'manage_options', 'lbgs-setting', array(&$this, 'lbgs_plugin_settings_page'));
      add_action('admin_init', array(&$this, 'lbgs_register_plugin_settings'));
   }

   public function lbgs_register_plugin_settings() {
      register_setting('lbgs-plugin-settings-group', OPT_GMAPS_KEY);
   }

   public function lbgs_plugin_settings_page() {
      ?>
      <div class="wrap">

         <h1>LB Geo Shop</h1>

         <form method="post" action="options.php">
            <?php settings_fields('lbgs-plugin-settings-group'); ?>
            <?php do_settings_sections('lbgs-plugin-settings-group'); ?>
            <table class="form-table">

               <tr valign="top">
                  <th scope="row">Google Maps Api Key</th>
                  <td>
                     <input type="text" name="<?php echo OPT_GMAPS_KEY; ?>" value="<?php echo esc_attr(get_option(OPT_GMAPS_KEY)); ?>" style="width: 50%;">
                  </td>
               </tr>
               
            </table>

      <?php submit_button(); ?>

         </form>
      </div>
      <?php
   }

}

$LBGS_SettingPage = new LBGS_SettingPage();
