=== LB Geo Shop ===
Contributors: Bonny85
Tags: geolocalizzazione, shop
Requires at least: 4.6
Tested up to: 4.6
Stable tag: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.txt

LB Geo Shop permette di aggiungere al vostro blog, una mappa dei punti vendita dei vostri prodotti.

== Description ==

LB Geo Shop permette di aggiungere al vostro blog, una mappa dei punti vendita dei vostri prodotti mediante uno shortcode.

**Instance:** [geo-shop-map]

* *width*: (default "700px") definisce la lunghezza della mappa.
* *height*: (default "500px") definisce la larghezza della mappa.
* *geoloc*: (default "Y") se Y attiva la geolocalizzazione dell'utente.
* *icon*: (default "") definisce l'icona del markers.

Esempio di shortcode:  [geo-shop-map geoloc="N" icon="htto://yourpath/icon.png"]

**API Reference**

*Select*  Class

Implementa una suite di funzioni per interagire con la basi di dati (aggiornato a fine 2012), in particolare le tabelle regioni, province, comuni.

*Select_Load*   Class

Implementa un metodo *populate()* per selezionare dei valori predefiniti nelle select. Per esempio se si usa il plugin per aggiungere un campo opzionale al profilo utente, “luogo di nascita”, dopo in primo salvataggio di tale informazione negli update successivi l’utente dovrà visualizzare le informazioni che si erano selezionate la volta prima.

[Documentation and example using (http://www.lucabonaldo.it/lb-geo-shop-plugin/)

== Installation ==

1. Upload LB-Geo-Shop nella directory `/wp-content/plugins/`.
2. Attivare il plugin tramite il menu 'Plugins' in WordPress.
3. Inserire la vostra Api Key di Google Maps. 
4. Posizionere gli shortcode in qualsiasi post o page, ecc.

**Per l'installazione multisito attivare il plugin nella rete.**

== Screenshots ==

1. Descrizione visuale degli attributi opzionali dello shortcode.

2. Esempio d'uso in un custom post type.