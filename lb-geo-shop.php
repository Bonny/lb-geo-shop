<?php

/**
 * Plugin Name: LB Geo shop
 * Plugin URI:
 * Description: 
 * Author: Luca Bonaldo <luca.bonaldo.lb@gmail.com>
 * Version: 1.0.0
 * Stable tag: 1.0.0
 * License: GPL v3 or later - http://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: GEOSHOP
 * Domain Path: /lang
 */
define("ADDRESS_TABLE", "lbgs_addresses");
define("SHOP_POST_TYPE", "geo-shop");

define("OPT_GMAPS_KEY", "lbgs_gmap_key");

define("TEXT_DOMAIN", "GEOSHOP");
define("PLUGIN_DIR", plugin_dir_url( __FILE__ ));

include 'class/LBGS_SettingPage.class.php';
include 'class/LBGS_PostType.class.php';
include 'class/LBGS_ShortcodeMap.class.php';

function lbgs_load_lang() {
   $currentLocale = get_locale();
   if (!empty($currentLocale)) {
      $moFile = dirname(__FILE__) . "/lang/geo-shop-" . $currentLocale . ".mo";
      if (@file_exists($moFile) && is_readable($moFile)) {
         load_textdomain(TEXT_DOMAIN, $moFile);
      }
   }
}

add_filter('init', 'lbgs_load_lang');

function lbgs_plugin_action_links($links) {
   $links[] = '<a href="' . esc_url(get_admin_url(null, 'options-general.php?page=lbgs-setting')) . '">' . __("Configura", TEXT_DOMAIN) . '</a>';
   return $links;
}

add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'lbgs_plugin_action_links');

/* ======== INIT PLUGIN ======== */

//if (!function_exists('lbgs_update_db_check')) {
//
//   function lbgs_update_db_check() {
//
//      global $wpdb;
//
//      $sql = "CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . ADDRESS_TABLE . " ("
//              . "  id BIGINT NOT NULL AUTO_INCREMENT, "
//              . "  post_id BIGINT NOT NULL,   "
//              . "  lat FLOAT(10, 6) NOT NULL, "
//              . "  lng FLOAT(10, 6) NOT NULL, "
//              . "  PRIMARY KEY (id)"
//              . ");";
//
//      $wpdb->query($sql);
//   }
//
//   add_action('plugins_loaded', 'lbgs_update_db_check');
//}
